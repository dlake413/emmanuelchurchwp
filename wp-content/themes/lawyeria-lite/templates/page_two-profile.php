		<?php
		/**
		 *  The template for displaying Page Blog..
		 *
		 *  @package lawyeria-lite
		 *
		 *	Template Name: Profile
		 */
		get_header();
		?>
			<section class="wide-nav">
				<div class="wrapper">
					<h3>
						<?php the_title(); ?>
					</h3><!--/h3-->
				</div><!--/div .wrapper-->
			</section><!--/section .wide-nav-->
		</header><!--/header-->
		<section id="content">
			<div class="wrapper cf">
				<div id="sidebar-left">
					<div class="widget widget_nav_menu">
						<div class="title-widget">교회안내</div>
						<?php
						wp_nav_menu( array( 
							'theme_location' => 'profile-menu', 
							'menu_class' => 'menu',
							'menu_id' => 'menu-profiles') ); 
						?>
					</div>
				</div>
				<div id="posts" class="right">
					<div id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					</div>
				</div><!--/div #posts-->
			</div><!--/div .wrapper-->
		</section><!--/section #content-->
		<?php get_footer(); ?>