<?php
	/**
	 *  The template for displaying Page Blog..
	 *
	 *  @package lawyeria-lite
	 *
	 *	Template Name: 메인
		*/
	get_header();
?>
</header>
<section id="content">
	<div class="wrapper cf">
		<div id="posts">
		</div><!--/div #posts-->
	</div><!--/div .wrapper-->
</section><!--/section #content-->
<?php get_footer(); ?>