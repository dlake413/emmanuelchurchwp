<?php
/**
 *  The template for displaying Sidebar.
 *
 *  @package lawyeria-lite
 */
?>
<aside id="sidebar-left">
	<?php dynamic_sidebar( 'right-sidebar' ); ?>
</aside><!--/aside #sidebar-right-->