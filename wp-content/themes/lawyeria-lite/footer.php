		<?php
		/**
		 *  The template for displaying Footer.
		 *
		 *  @package lawyeria-lite
		 */
		?>
		<footer id="footer">
			<div class="wrapper cf">
				<div id="footer-contact-info">
					<div class="title block">
						땅끝까지 끝날까지 세계선교하는 필라 임마누엘 교회(PCA)
					</div><br>
					<div class="block">
						<i class="fas fa-map-marker"></i> 4723 spruce st, Philadelphia, PA 19139
					</div>
					<div class="block">
						<i class="fas fa-phone"></i> (215)476-0330
					</div>
					<div class="block">
						<a href="mailto:churchoffice@iemmanuel.org"><i class="fas fa-envelope"></i> churchoffice@iemmanuel.org</a>
					</div>
				</div>
			</div><!--/div .wrapper .cf-->
		</footer><!--/footer #footer-->
		<?php wp_footer(); ?>
	</body>
</html>
